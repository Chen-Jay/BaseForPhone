<?php
//addDepartment(166,16,"2016级6班");
//addUser(201636664165,'陈奕霖',123456,165,0);	
//deleteUser(201630600000);
//$base64='';
//addPictue(2,$base64);
//reboot();
//deviceData();
updateUser('201630664055');
/**
 * 向考勤机添加/更新部门（id=>部门id，pid=>上级部门id，name=>部门名）
 */
function addDepartment($id,$pid,$name)
{
	$order=array(
				'id'=>1004,		//命令id（不重复即可）
				'do'=>'update',
				'data'=>'dept',	
				'dept'=> array(
				array(
					'id'=>"{$id}",	//部门id
					'pid'=>"{$pid}", //上级部门id
					'name'=>$name //部门名
				)
				)
				);
	$command=json_encode($order,JSON_UNESCAPED_UNICODE);
	postToDatabase($command);
}
/**
 * 向考勤机添加/更新用户
 * ccid=>用户登录账号，name=>用户名，pw=>登录密码，class=>用户班级，auth=>用户权限（0：普通员工，14：管理员）
 */
function addUser($ccid,$name,$pw,$class,$auth)
{
	$order=array(
				'id'=>'1001',		//命令id（不重复即可）
				'do'=>'update',
				'data'=>'user',		
				'ccid'=>$ccid,		//员工账号
				'name'=>$name,		//员工姓名
				'passwd'=>md5($pw),	//员工登陆密码
				'card'=>'11111',	//员工卡号（预留字段，无用）
				'deptid'=>$class,	//部门编号（默认为5班）
				'auth'=>$auth,			//设备权限（0=>普通，14=>管理员）
				'faceexist'=>0		//人脸识别（预留字段，无用）
	);
	$command=json_encode($order,JSON_UNESCAPED_UNICODE);
	postToDatabase($command);
}
/**
 * 向考勤机添加封面图片
 * index=>图片编号,pic=>图片base64码
 */
function addPictue($index,$pic)
{
	$order=array(
			'id'=>1005,
			'do'=>'update',
			'data'=>'advert',
			'index'=>$index,	//图片编号
			'advert'=>$pic	//图片的base64码
	);
	$command=json_encode($order);
	postToDatabase($command);
}
/**
 * 重启考勤机
 */
function reboot()
{
	$order=array(
		'id'=>1010,
		'do'=>'cmd',
		'cmd'=>'reboot'
	);
	$command=json_encode($order);
	postToDatabase($command);
}
/**
 * 清除掉设备中的所有数据
 */
function cleanData()
{
	$order=array(
		'id'=>1010,
		'do'=>'delete',
		'data'=>array(
			'user',
			'fingerprint',
			'face',
			'headpic',
			'clockin',
			'pic',
			'dept'
		)
	);
	$command=json_encode($order);
	postToDatabase($command);
}
/**
 * 上传考勤机的设备信息
 */
function deviceData()
{
	$order=array(
		'id'=>1010,
		'do'=>'upload',
		'data'=>'info'
	);
	$command=json_encode($order);
	postToDatabase($command);
}
/**
 * 删除掉指定员工的信息
 * ccid=>员工的账号
 */
function deleteUser($ccid)
{
	$order=array(
		'id'=>1006,
		'do'=>'delete',
		'data'=>array(
			'user',
			'fingerprint',
			'face',
			'headpic',
			'clockin',
			'pic'
		),
		'ccid'=>array(
			$ccid
		)
	);
	$command=json_encode($order);
	postToDatabase($command);
}
/**
 * 上传指定用户的各种信息
 */
function updateUser($Id)
{
	$order=array(
		'id'=>1006,
		'do'=>'upload',
		'data'=>array(
			'user','fingerprint','headpic','clockin','pic'
		),
		'ccid'=>$Id
	);
	$command=json_encode($order);
	postToDatabase($command);
}

function postToDatabase($order)
{
	$db=getDatabase();
	$sql="INSERT INTO kqj_order(command) VALUES(?)";
	$db->prepare($sql)->execute(array($order));
}

function getDatabase()
{
	$db=new PDO("mysql:dbname=kqj;port=3306;host=127.0.0.1;charset=utf8mb4","kqj","kqjpass");
	return $db;
}